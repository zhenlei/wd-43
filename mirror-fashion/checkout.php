<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<title>Mirror Fashion</title>

		<link rel="stylesheet" type="text/css" href="css/reset.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/checkout.css">
		<link rel="stylesheet" type="text/css" href="css/mobile.css" media="(max-width: 320px)">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="jumbotron">
					<div class="container">
						<h1>Ótima escolha!</h1>
						<p>Obrigado por comprar na Mirror Fashion! Preencha seus dados para efetivar a compra.</p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Sua compra</h2>
						</div>
						<div class="panel-body">
							<dl>
								<dt>Cor</dt>
								<dd><?= $_POST['cor'] ?></dd>

								<dt>Tamanho</dt>
								<dd><?= $_POST['tamanho'] ?></dd>

								<dt>Produto</dt>
								<dd><?= $_POST['nome'] ?></dd>

								<dt>Preço</dt>
								<dd id="preco"><?= $_POST['preco'] ?></dd>
							</dl>
							<div class="form-group">
								<label for="qt">Quantidade</label>
								<input id="qt" class="form-control" type="number" min="0" max="99" value="1">
							</div>
							<div class="form-group">
								<label for="total">Total</label>
								<output for="qt valor" id="total" class="form-control">
									<?= $_POST["preco"] ?>
								</output>
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-8">
					<form>
						<div class="row">
							<fieldset class="col-md-6">
								<legend>Dados pessoais</legend>
								<div class="form-group">
									<label for="nome">Nome completo</label>
									<input type="text" class="form-control" id="nome" name="nome" autofocus required>
								</div>	

								<div class="form-group">
									<label for="email">Email</label>
									<div class="input-group">
										<span class="input-group-addon">@</span>
										<input type="text" class="form-control" id="email" name="email" placeholder="email@example.com" required>
									</div>
								</div>

								<div class="form-group">
									<label for="cpf">CPF</label>
									<input type="text" class="form-control" id="cpf" name="cpf" placeholder="000.000.000-00" required>
								</div>

								<div class="checkbox">
									<label>
										<input type="checkbox" value="sim" name="spam" checked>
										Quero recber spam da Mirror Fashion
									</label>
								</div>	
							</fieldset>

							<fieldset class="col-md-6">
								<legend>Cartão de crédito</legend>
								<div class="form-group">
									<label for="numero-cartao">Número - CVV</label>
									<input type="text" class="form-control" id="numero-cartao" name="numero-catao">
								</div>

								<div class="form-group">
									<label for="bandeira-cartao">Bandeira</label>
									<select name="bandeira-cartao" id="bandeira-cartao" class="form-control">
										<option value="master">MasterCard</option>
										<option value="visa">Visa</option>
										<option value="amex">American Express</option>
									</select>
								</div>

								<div class="form-group">
									<label for="validade-cartao">Validade</label>
									<input type="month" class="form-control" id="validade-cartao" name="validade-catao">
								</div>
							</fieldset>

							<button type="submit" class="bt btn-primary btn-lg pull-right"><span class="glyphicon glyphicon-thumbs-up" ></span>Confirmar Pedido</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<script src="js/converteMoeda.js"></script>
		<script src="js/testaConversao.js"></script>
		<script src="js/total.js"></script>
	</body>
</html>